package org.alpana.jms.cardmgmt.alerts;

import org.alpana.jms.cardmgmt.model.Card;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.time.format.DateTimeFormatter;

public class SharedConsumer {

    public static void main(String[] args) throws NamingException {
        System.out.println("Shared Alerts Consumer App");
        InitialContext initialContext = new InitialContext();
        Topic cardTopic = (Topic) initialContext.lookup("topic/cardTopic");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()){
            jmsContext.setClientID("alertsApp");
            JMSConsumer consumer1 = jmsContext.createSharedConsumer(cardTopic, "sharedSubscription");
            JMSConsumer consumer2 = jmsContext.createSharedConsumer(cardTopic, "sharedSubscription");

            for (int i = 0; i < 5; i++) {
                Card receivedCard1 = consumer1.receiveBody(Card.class);
                System.out.println("--------------- Consumer 1: Details of received Card ---------------");
                receivedCard1.displayInfo();
                System.out.println("--------------------------------------------------------------------");
                System.out.println("Card Transaction Alert!");

                Card receivedCard2 = consumer2.receiveBody(Card.class);
                System.out.println("--------------- Consumer 2: Details of received Card  ---------------");
                receivedCard2.displayInfo();
                System.out.println("---------------------------------------------------------------------");
                System.out.println("Card Transaction Alert!");
            }
        }
    }

}
