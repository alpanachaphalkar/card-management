package org.alpana.jms.cardmgmt.alerts;

import org.alpana.jms.cardmgmt.model.Card;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

import javax.jms.JMSContext;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class Consumer {

    public static void main(String[] args) throws NamingException {
        System.out.println("Alerts Consumer App");
        InitialContext initialContext = new InitialContext();
        Topic cardTopic = (Topic) initialContext.lookup("topic/cardTopic");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()){
            Card receivedCard = jmsContext.createConsumer(cardTopic).receiveBody(Card.class);
            System.out.println("--------------- Details of received Card ---------------");
            receivedCard.displayInfo();
            System.out.println("--------------------------------------------------------");
            System.out.println("Card Transaction Alert!");
        }
    }
}
