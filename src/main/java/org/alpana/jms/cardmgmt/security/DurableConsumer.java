package org.alpana.jms.cardmgmt.security;

import org.alpana.jms.cardmgmt.model.Card;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.time.format.DateTimeFormatter;

public class DurableConsumer {

    public static void main(String[] args) throws NamingException, InterruptedException {
        System.out.println("Security Durable Consumer App");
        InitialContext initialContext = new InitialContext();
        Topic cardTopic = (Topic) initialContext.lookup("topic/cardTopic");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()){
            jmsContext.setClientID("securityApp"); // Mandatory for durable consumer
            JMSConsumer durableConsumer = jmsContext.createDurableConsumer(cardTopic, "durableSubscription");
            durableConsumer.close();

            Thread.sleep(12000);

            durableConsumer = jmsContext.createDurableConsumer(cardTopic, "durableSubscription");
            Card receivedCard = durableConsumer.receiveBody(Card.class);
            System.out.println("--------------- Details of received Card ---------------");
            receivedCard.displayInfo();
            System.out.println("--------------------------------------------------------");
            System.out.println("Security Alert!");
        }
    }
}
