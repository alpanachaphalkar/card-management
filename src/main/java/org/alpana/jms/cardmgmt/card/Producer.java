package org.alpana.jms.cardmgmt.card;

import org.alpana.jms.cardmgmt.model.Card;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

import javax.jms.JMSContext;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class Producer {

    public static void main(String[] args) throws NamingException {
        System.out.println("Card Producer App");
        InitialContext initialContext = new InitialContext();
        Topic cardTopic = (Topic) initialContext.lookup("topic/cardTopic");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()) {
            Card card = new Card();
            card.setCardNumber("4210244607136126");
            card.setCardHolderFirstName("Abc");
            card.setCardHolderLastName("Xyz");
            card.setCvc("123");
            card.setCardProvider("VISA");
            YearMonth expiration = YearMonth.parse("10/23", DateTimeFormatter.ofPattern("MM/yy"));
            card.setExpiration(expiration);
            jmsContext.createProducer().send(cardTopic, card);
            System.out.println("Card Sent to Topic");
        }
    }
}
