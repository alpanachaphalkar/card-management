package org.alpana.jms.cardmgmt.model;

import java.io.Serializable;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class Card implements Serializable {

    private String cardHolderFirstName;
    private String cardHolderLastName;
    private String cardNumber;
    private String cvc;
    private String cardProvider;
    private YearMonth expiration;

    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardProvider() {
        return cardProvider;
    }

    public void setCardProvider(String cardProvider) {
        this.cardProvider = cardProvider;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public YearMonth getExpiration() {
        return expiration;
    }

    public void setExpiration(YearMonth expiration) {
        this.expiration = expiration;
    }

    public void displayInfo(){
        System.out.println("Card Holder Name: " +  this.cardHolderFirstName + " " + this.cardHolderLastName);
        System.out.println("Card number: " + this.cardNumber);
        System.out.println("Card CVC: " + this.cvc);
        System.out.println("Card Provider: " + this.cardProvider);
        System.out.println("Card Expiry Date: " + this.expiration.format(DateTimeFormatter.ofPattern("MM/yy")));
    }
}
